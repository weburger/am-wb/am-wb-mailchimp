'use strict';

angular.module('ngMaterialWeburgerMailchimp')
/**
 * @ngdoc function
 * @name digidociMainApp.controller:DialogmodelCtrl
 * @description # DialogmodelCtrl Controller of the digidociMainApp
 */
.controller('AmWbMailchimpSubscribeCtrl', function($log, $http, $scope, $rootScope) {

	var wbModel = $scope.wbModel;
//	wbModel.username = 'dpq';
//	wbModel.dc = 'us16';
//	wbModel.u = '5c05046d30a51df328ea66bfa';
//	wbModel.id = '082cc6255c';


	// Create a resource for interacting with the MailChimp API
	function addSubscription (mailchimp)  {
		var params = {};
		var url = '//' + wbModel.username + '.' + wbModel.dc +	'.list-manage.com/subscribe/post-json';

		var fields = Object.keys(mailchimp);
		for(var i = 0; i < fields.length; i++) {
			params[fields[i]] = mailchimp[fields[i]];
		}
		params.u = wbModel.u;
		params.id = wbModel.id;

		// Send subscriber data to MailChimp
		$http({
		     url: url,
		     method: 'JSONP',
		     params: params,
		     jsonpCallbackParam: 'c'
		})
		// Successfully sent data to MailChimp.
		.then(function (res) {
			var response = res.data;
			// Define message containers.
			mailchimp.errorMessage = '';
			mailchimp.successMessage = '';

			// Store the result from MailChimp
			mailchimp.result = response.result;

			// Mailchimp returned an error.
			if (response.result === 'error') {
				if (response.msg) {
					// Remove error numbers, if any.
					var errorMessageParts = response.msg.split(' - ');
					if (errorMessageParts.length > 1)
						errorMessageParts.shift(); // Remove the error number
					mailchimp.errorMessage = errorMessageParts.join(' ');
				} else {
					mailchimp.errorMessage = 'Sorry! An unknown error occured.';
				}
			}
			// MailChimp returns a success.
			else if (response.result === 'success') {
				mailchimp.successMessage = response.msg;
			}

			//Broadcast the result for global msgs
			$rootScope.$broadcast('mailchimp-response', response.result, response.msg);
		},// Error sending data to MailChimp
		function (error) {
			$log.error('MailChimp Error: %o', error);
		});
	}

	// Handle clicks on the form submission.
	$scope.addSubscription = addSubscription;
});